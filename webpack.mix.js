const mix = require('laravel-mix');
const webpack = require('webpack');

mix.setPublicPath('dist/')
    .js('resources/js/senses-builder.js', 'dist/js')
   .sass('resources/sass/senses-builder.scss', 'dist/css');
