<?php

namespace Senses\Builder\Console\Commands;

use Senses\Builder\Models\TemplateBlock;
use Illuminate\Console\Command;
use Illuminate\Support\Str;

class SeedTemplateBlocks extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'builder:seed_template_blocks';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Will seed template blocks, this will overwrite existing template blocks';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        TemplateBlock::truncate(); //@todo optional

        //@todo add support for what component set to use via config
        $components = 'bootstrap';
        $componentFunction = $components . 'Components';
        $categories = [];
        if(method_exists($this, $componentFunction)) {
            $categories = $this->$componentFunction();
        }

        foreach($categories as $category => $templates) {
            foreach($templates as $template) {
                //check if template exists.
                //check if image exists.
                $title = Str::title(str_replace('-', ' ',$template));

                TemplateBlock::create([
                    'title' => $title, 
                    'stored_title' => $template, 
                    'category' => $category
                ]);
            }
        }
    }


    public function bootstrapComponents() {
        return [
            'Headers' => [
                'header-h1',
                'header-h1-subheader',
                'header-h2',
                'header-h2-subheader',
                'header-h3',
                'header-h3-subheader',
            ],
            'Text & Paragraph' => [
                'one-column-heading-large-text',
                'one-column-heading-medium-text',
                'one-column-heading-small-text',
                'paragraph',
                'two-column-heading-text',
                'two-column-heading-text-left-image-right',
                'two-column-heading-text-right-image-left',
                'two-column-heading-text-left-image-large-right',
                'two-column-heading-text-right-image-large-left',
            ],
            'Cards' => [
                'one-card',
                'two-card',
                'two-card-rounded-image-header-text',
                'three-card',
            ],
            'Buttons' => [
                'one-button',
                'two-button'
            ],
            'Separators & Spacers' => [
                'spacer-hr',
                'spacer-large',
                'spacer-medium',
                'spacer-small'
            ],

            'Images' => [
                'one-column-image',
                'two-column-image',
                'two-column-image-text',
                'three-column-image',
                'three-column-image-text',
                'four-column-image-left-text-right'
            ],
            'Quotes' => [
                'one-column-block-quote',
                'two-column-block-quote',
                'one-column-block-quote-image-rounded',
            ],
            'Call to Action' => [
                'one-column-call-to-action',
                'two-column-call-to-action-image-left',
                'two-column-call-to-action-image-right'
            ],
        ];
    }

    public function tailwindComponents() {
        return [];
    }

    public function mailcoachComponents() {
        return [];
    }
}
