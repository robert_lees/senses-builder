<?php
namespace Senses\Builder;

use Illuminate\Support\Facades\Storage;
use PHPHtmlParser\Dom;

class BuilderBlock {
    protected $template;
    protected $fieldStructure;
    protected $value;

    protected $html = "";
    protected $css = "";

    public function __construct($template, $value = null)
    {
        $this->template = $template;
        $this->value = $value;
    }

    public function process() {
        $blockContents = file_get_contents(resource_path('js\vendor\senses-builder\components\blocks\\' . $this->template . '.vue'));

        $html = $this->findInTemplate('#<template>(.*?)</template>#s', $blockContents);
        $css = $this->findInTemplate('#<style>(.*?)</style>#s', $blockContents);

        $this->fieldStructure = $this->getFieldStructure($blockContents, $this->template);
        $fields = $this->getFields($this->value, $this->fieldStructure);
        foreach($fields as $field => $structure) {
            $html = $this->replaceTags($field, $structure, $html);
            $html = $this->replaceAttributes($field, $structure, $html);
            $html = $this->replaceStyles($field, $structure, $html);
        }
        $html = $this->processDom($html, $fields);
        $this->html = $html;
        $this->css = $css;
    }

    public function replaceTags($field, $structure, $html) {
        if($structure['type'] == "group") {
            foreach($structure['value'] as $childField => $childStructure) {
                $html = $this->replaceTags($childField, $childStructure, $html);
            }
            return $html;
        }
        else if($structure['type'] == "image") {
            return $html;
        }
        return str_replace("{{ fields.{$field}.value }}", $structure['value'], $html);  //replace {{}}
    }

    public function replaceAttributes($field, $structure, $html) {
        if($structure['type'] == "image") {
            $html = str_replace(":alt='fields.{$field}.value.alt'", "alt='" . $structure['value']['alt'] . "'", $html);
            $html = str_replace(":alt=\"fields.{$field}.value.alt\"", "alt='" . $structure['value']['alt'] . "'", $html);
            $html = str_replace(":title='fields.{$field}.value.alt'", "title='" . $structure['value']['alt'] . "'", $html);
            $html = str_replace(":title=\"fields.{$field}.value.alt\"", "title='" . $structure['value']['alt'] . "'", $html);
            $html = str_replace(":src='fields.{$field}.value.src'", "src='" . $structure['value']['src'] . "'", $html);
            $html = str_replace(":src=\"fields.{$field}.value.src\"", "src='" . $structure['value']['src'] . "'", $html);
        }
        return $html;
    }

    public function replaceStyles($field, $structure, $html) {
        if($this->isStyleField($structure['type'])) {
            $style = trim($this->getStyleRules($field, $structure));

            $html = str_replace(":style='setStyle(\"{$field}\")'", "style='{$style}'", $html);
            $html = str_replace(":style=\"setStyle('{$field}')\"", "style='{$style}'", $html);
        }

        return $html;
    }

    public function findInTemplate($regex, $template) {
        $found = "";
        preg_match($regex, $template, $matches);
        if(isset($matches[1])) {
            $found = $matches[1];
        }

        return $found;
    }

    //Grab Fields from Vuejs data, these fields are expected to be wrapped in //FIELDS_START and //FIELDS_END
    public function getFieldStructure($blockFile, $template = 'unknown') {
        $fieldStructure = $this->findInTemplate('#\/\/FIELDS\_START(.*?)\/\/FIELDS\_END#s', $blockFile);

        $fieldStructure = '{' . $fieldStructure . '}';

        $fieldStructure = trim(str_replace("\xc2\xa0", ' ', $fieldStructure));
        $fieldStructure = preg_replace('/\s\s+/', ' ', $fieldStructure);
        $fieldStructure = $this->fixJson($fieldStructure);
        $fieldStructure = json_decode($fieldStructure);

        if(!$fieldStructure) {
            throw new \Exception('Invalid field json in block in ' . $template);
        }

        $fieldStructure = json_decode(json_encode($fieldStructure), true); //ensure structure is an array.

        if(is_array($fieldStructure) && $fieldStructure['fields']) {
            return $fieldStructure['fields'];
        }

        return [];
    }


    //Map structure to key used in vue aka dot notation.   group.field => structure or field => structure
    public function getFlattenedFieldStructure(array $fieldStructure, $prefix = '') {
        $flattenedFieldStructure = [];

        foreach($fieldStructure as $field => $structure) {
            if($structure['type'] == 'group') {
                $flattenedFieldStructure = array_merge($flattenedFieldStructure, $this->getFlattenedFieldStructure($structure['value'], $field . '.'));
            }

            $flattenedFieldStructure[$prefix . $field] = $structure;
        }
        return $flattenedFieldStructure;
    }

    public function getFieldType($field, array $flattenedFieldStructure) {
        if(!isset($flattenedFieldStructure[$field])) {
            return null;
        }
        return $flattenedFieldStructure[$field]['type'];
    }

    //Combines block field structure, with the user saved values.
    public function getFields(array $data, array $fieldStructure) {
        $flattenedFieldStructure = $this->getFlattenedFieldStructure($fieldStructure);
        foreach($data as $field => $value) {
            $fieldType = $this->getFieldType($field, $flattenedFieldStructure);
            if($fieldType == 'group') {
                $flattenedFieldStructure[$field]['value'] = $this->getFields($value, $fieldStructure[$field]['value']);
            }
            else {
                $flattenedFieldStructure[$field]['value'] = $value;
            }
        }

        return $flattenedFieldStructure;
    }



    //Fix vue style key:"value" into json "key":"value
    //https://stackoverflow.com/questions/6250815/parsing-json-missing-quotes-on-name
    public function fixJson( $j ){
        $j = trim( $j );
        $j = ltrim( $j, '(' );
        $j = rtrim( $j, ')' );
        $a = preg_split('#(?<!\\\\)\"#', $j );
        for( $i=0; $i < count( $a ); $i+=2 ){
            $s = $a[$i];
            $s = preg_replace('#([^\s\[\]\{\}\:\,]+):#', '"\1":', $s );
            $a[$i] = $s;
        }
        //var_dump($a);
        $j = implode( '"', $a );
        //var_dump( $j );
        return $j;
    }

    public function processDom($html, $fields) {
        $dom = new Dom();
        $dom->load($html);

        $this->replaceHtml($dom, $fields);
        $this->replaceHref($dom, $fields);
        $this->replaceCss($dom, $fields);

        return $dom->__toString();
    }

    public function replaceHtml($dom, $fields) {
        $elements = $dom->find('[v-html]');
        if($elements) {
            foreach ($elements as $element) {
                $this->replaceHtmlAttributes($element, $fields);
            }
        }
    }

    public function replaceHtmlAttributes($element, $fields) {
        $fieldKey = $element->getAttribute('v-html');
        $fieldKey = str_replace('fields.', '', $fieldKey);
        $fieldKey = str_replace('.value', '', $fieldKey);
        if (isset($fields[$fieldKey])) {
            $childDom = new Dom();
            $childDom->load($fields[$fieldKey]['value']);
            $element->addChild($childDom->root);
        }
        $element->removeAttribute('v-html');

        $childElements = $element->find('[v-html]');
        if($childElements) {
            foreach ($childElements as $childElement) {
                $this->replaceHtmlAttributes($childElement, $fields);
            }
        }
    }

    public function replaceHref($dom, $fields) {
        $elements = $dom->find('[:href]');
        if($elements) {
            foreach ($elements as $element) {
                $this->replaceHrefAttributes($element, $fields);
            }
        }
    }

    public function replaceHrefAttributes($element, $fields) {
        $fieldKey = $element->getAttribute(':href');
        $fieldKey = str_replace('fields.', '', $fieldKey);
        $fieldKey = str_replace('.value', '', $fieldKey);
        if (isset($fields[$fieldKey])) {
            $href = $fields[$fieldKey]['value'];
            $element->setAttribute('href', $href);
        }
        $element->removeAttribute(':href');

        $childElements = $element->find('[:href]');
        if($childElements) {
            foreach ($childElements as $childElement) {
                $this->replaceHrefAttributes($childElement, $fields);
            }
        }
    }

    public function replaceCss($dom, $fields) {
        $elements = $dom->find('[:class]');
        if($elements) {
            foreach ($elements as $element) {
                $this->replaceCssAttributes($element, $fields);
            }
        }
    }

    public function replaceCssAttributes($element, $fields) {
        $fieldKey = $element->getAttribute(':class');
        $fieldKey = str_replace('fields.', '', $fieldKey);
        $fieldKey = str_replace('.value', '', $fieldKey);

        if (isset($fields[$fieldKey])) {
            $class = $element->getAttribute('class');
            $class .= " " . $fields[$fieldKey]['value'];
            $element->setAttribute('class', $class);
        }

        $element->removeAttribute(':class');

        $childElements = $element->find('[:class]');
        if($childElements) {
            foreach ($childElements as $childElement) {
                $this->replaceCssAttributes($childElement, $fields);
            }
        }
    }

    public function isStyleField($fieldType, $allowGroups = true) {
        $styleFields = ['color', 'backgroundColor'];
        if($allowGroups) {
            array_push($styleFields, 'group');
        }

        return in_array($fieldType, $styleFields);
    }

    public function getStyleRules($field, $structure) {
        $rules = "";
        if($structure['type'] == 'group') {
            foreach($structure['value'] as $childField => $childStructure) {
                if($this->isStyleField($childStructure['type'])) {
                    $rules .= $this->getStyleRules($field, $childStructure);
                }
            }
        }
        else {
            $rules .= kebab_case($structure['type']) . ": " . $structure['value'] . "; ";
        }

        return $rules;
    }

    public function getHtml() {
        return $this->html;
    }

    public function getCss() {
        return $this->css;
    }
}