<?php
namespace Senses\Builder;

use Illuminate\Support\Facades\Route;
use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Schema\Blueprint;
use Senses\Builder\Console\Commands\SeedTemplateBlocks;

class BuilderServiceProvider extends ServiceProvider
{
    public function boot()
    {
        $this->loadViewsFrom(__DIR__.'/../resources/views', 'senses-builder');

        if ($this->app->runningInConsole()) {
            $this->publishViews();
            $this->publishAssets();
            $this->publishMigrations();
            $this->publishConfig();
            $this->publishCommands();
        }

        Route::macro('sensesBuilder', function (string $url = '') {
            Route::prefix($url)->group(function () {
                $middlewareClasses = config('senses-builder.middleware', []);
                Route::middleware($middlewareClasses)->prefix('')->group(__DIR__ . '/../routes/api.php');
            });
        });

        Blueprint::macro('sensesBuilderFields', function () {
            $this->string('builder_status')->index()->default('draft');
            $this->longText('builder_html')->nullable();
            $this->longText('builder_css')->nullable();
        });
    }

    public function register()
    {
        $this->mergeConfigFrom(
            __DIR__.'/Config/senses-builder.php', 'senses-builder'
        );
    }

    public function publishViews() {
        $this->loadViewsFrom(__DIR__.'/../resources/views', 'builder');

        $this->publishes([
            __DIR__.'/../resources/views' => base_path('resources/views/vendor/senses-builder'),
        ], 'senses-builder-views');
    }

    public function publishAssets() {
                    $this->publishes([
                __DIR__.'/../resources/js' => base_path('resources/js/vendor/senses-builder'),
                __DIR__.'/../resources/sass' => base_path('resources/sass/vendor/senses-builder'),
                __DIR__.'/../resources/images' => base_path('public/senses-builder/images'),
            ], 'senses-builder-assets');
    }

    public function publishMigrations() {
        if (!class_exists('CreateSensesBuilderTables')) {

            $publishes = [
                __DIR__ . '/../database/migrations/create_senses_builder_tables.php.stub' => database_path('migrations/' . date('Y_m_d_His', time()) . '_create_senses_builder_tables.php')
            ];

            if(config('senses-builder.enable_files', true)) {
                $publishes[ __DIR__ . '/../database/migrations/create_senses_builder_files_table.php.stub'] = database_path('migrations/' . date('Y_m_d_His', time()) . '_create_senses_builder_files_table.php');
            }

            $this->publishes($publishes, 'senses-builder-migrations');
        }
    }

    public function publishConfig() {
        $this->publishes([
            __DIR__.'/Config/senses-builder.php' => config_path('senses-builder.php'),
        ], 'senses-builder-config');

    }

    public function publishCommands() {
        $this->commands([
            SeedTemplateBlocks::class,
        ]);
    }
}