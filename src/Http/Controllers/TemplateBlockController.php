<?php

namespace Senses\Builder\Http\Controllers;


use Senses\Builder\Http\Resources\TemplateBlockResource;
use Senses\Builder\Models\TemplateBlock;
use Illuminate\Http\Request;

class TemplateBlockController
{

    public function index(Request $request)
    {
        $filters = $request->input('filter', ['category' => '']);
        $templateBlocks = TemplateBlock::where('category', urldecode($filters['category']))->paginate($request->input('limit', 15));

        return TemplateBlockResource::collection($templateBlocks); //@todo remove resources, not big enough to justify extra files
    }

    public function listCategories(Request $request) {
        $categories = TemplateBlock::groupBy('category')->distinct()->get(['category'])->pluck('category');
        return response()->json([
            "object" => "senses-builder-categories",
            'categories' => $categories
        ]);
    }
}
