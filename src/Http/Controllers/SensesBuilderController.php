<?php

namespace Senses\Builder\Http\Controllers;

use Senses\Builder\Builder;
use Senses\Builder\Models\BuilderBlock;
use Senses\Builder\Http\Requests\StoreBuilderRequest;

class SensesBuilderController {

    public function store(StoreBuilderRequest $request) {

        $model = Builder::getModel($request->input('blockable_type'), $request->input('blockable_id'));

        $requestBlocks = $request->input('blocks');

        //just wipe previous version for now.
        $model->load('builderBlocks');

        //remove any unused blocks.
        $model->clearUnusedBuilderBlocks($requestBlocks);

        $builderBlocks = [];
        foreach($requestBlocks as $index => $block) {
            $block['order'] = $index;

            if(isset($model->builderBlocks[$index])) {
                $builderBlock = $model->builderBlocks[$index];
            }
            else {
                $builderBlock = new BuilderBlock();
            }

            $builderBlock->fill($block);
            array_push($builderBlocks, $builderBlock);
        }


        $model->builderBlocks()->saveMany($builderBlocks);
        $model->builder_status = $request->input('status_slug');
        $builder = new Builder($builderBlocks);
        $model->builder_html = $builder->getHtml();
        $model->builder_css = $builder->getCss();
        $model->save();

        return response()->json([
            'object' => 'senses-builder-action',
            'message' => 'Template saved successfully'
        ]);
    }
}


