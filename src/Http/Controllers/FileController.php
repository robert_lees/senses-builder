<?php

namespace Senses\Builder\Http\Controllers;

use Senses\Builder\Models\File;
use Illuminate\Http\Request;

class FileController
{

    public function index(Request $request)
    {
        $facilities = $this->query(File::class)->paginate($request->input('limit', 15));
        return FileResource::collection($facilities);
    }

    public function store(Request $request)
    {
        $data = $request->all();

        $file = new File();
        $data['fileable'] = null;

        if ($request->input('file_base64')) {
            $data = array_merge($data, File::storeBase64File($request->input('file_base64'), $request->input('title', 'file')));
        } else {
            $data = array_merge($data, File::storeFile($request->file('file')));
        }
        if (isset($data['fileable_type']) && isset($data['fileable_id'])) {
            $data['fileable'] = ('\App\Models\\' . studly_case($data['fileable_type']))::findOrFail($data['fileable_id']);
        }
        $previewFileData = File::generatePreview($data);
        if ($previewFileData) {
            $data['previewFile'] = File::storeFile($previewFileData);
        }

        $file->fileable()->associate($data['fileable']);

        if (isset($data['public']) && $data['public']) {

            $file->public_slug = uniqid('', true) . "." . $data['extension'];
        } else {
            $file->public_slug = null;
        }

        $file->fill($data);
        $file->save();
        return new FileResource($file);
    }
    
}
