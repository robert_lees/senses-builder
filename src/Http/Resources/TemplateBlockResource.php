<?php

namespace Senses\Builder\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class TemplateBlockResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return array_merge(parent::toArray($request), [
            "object"                => 'senses-builder-template-block',
            "image_url"             => $this->image_url, 
        ]);
    }
}
