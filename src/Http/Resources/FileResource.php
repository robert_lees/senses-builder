<?php

namespace Senses\Builder\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class FileResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return array_merge(parent::toArray($request), [
            "object"                => 'senses-builder-file',
            "files"                 => FileResource::collection($this->whenLoaded('files')),
        ]);
    }
}
