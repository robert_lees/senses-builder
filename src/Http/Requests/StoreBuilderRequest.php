<?php

namespace Senses\Builder\Http\Requests;

use Senses\Builder\Builder;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class StoreBuilderRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'blockable_id' => 'required|integer',
            'status_slug' => 'required|string|max:255',
            'blockable_type' => [
                'required',
                Rule::in(Builder::getModelKeys()),
            ],
            'blocks' => 'array',
            'blocks.*.template' => 'required|string|max:255',
            'blocks.*.value' => 'array'
        ];
    }
}
