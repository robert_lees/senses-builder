<?php

namespace Senses\Builder\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BuilderBlock extends Model {
    use SoftDeletes;

    public $table = 'senses_builder_blocks';
    protected $fillable = ['template', 'value', 'order'];
    protected $casts = ['value' => 'array'];

    public function blockable() {
        return $this->morphTo();
    }
}