<?php

namespace Senses\Builder\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TemplateBlock extends Model {
    use SoftDeletes;

    public $table = 'senses_builder_template_blocks';
    protected $fillable = ['title', 'stored_title', 'category'];
    protected $appends = ['image_url'];
    
    public function getImageUrlAttribute() {
        return asset('/senses-builder/images/blocks/' . $this->stored_title .'.jpg');
    }
}