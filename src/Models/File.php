<?php

namespace Senses\Builder\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class File extends Model {
    use SoftDeletes;
    
    public $table = 'senses_builder_files';
}