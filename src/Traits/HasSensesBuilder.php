<?php

namespace Senses\Builder\Traits;

use Senses\Builder\Builder;
use Senses\Builder\Models\BuilderBlock;
use Illuminate\Support\Facades\Route;
use Senses\Builder\Models\Status;

trait HasSensesBuilder {

    //Expects class to have builder_html, builder_css, builder_status_id

    // public function __construct(array $attributes = [])
    // {
    //     parent::__construct($attributes);
    //     array_push($this->appends, 'builder_data');
    // }

    public function builderBlocks() {
        return $this->morphMany(BuilderBlock::class, 'blockable');
    }

    public function getBuilderDataAttribute() {
        $blocks =  $this->builderBlocks->map(function ($user) {
            return $user->only(['template', 'value']);
        });



        return [
            'id' => $this->id,
            'type' => $type = Builder::getModelKey($this),
            'public_url' => $this->public_url,
            'blocks' => $blocks->toArray(),
            'status_slug' => $this->builder_status,
        ];
    }

    public function clearUnusedBuilderBlocks($requestBlocks) {
        $originalBlockCount = $this->builderBlocks->count();
        if(count($requestBlocks) < $originalBlockCount) {
            for($i = count($requestBlocks); $i <= $originalBlockCount; ++$i) {
                if(isset($this->builderBlocks[$i])) {
                    $this->builderBlocks[$i]->delete();
                }
            }
        }
    }

    public function scopeWhereBuilderDraft($query, $slug)
    {
        return $query->where('builder_status', 'draft');
    }

    public function scopeWhereBuilderPublished($query, $slug)
    {
        return $query->where('builder_status', 'published');
    }

    public function getPublicUrlAttribute() {
        $url = null;
        $type = Builder::getModelKey($this);
        if(Route::has($type . '.show')){
            $url = route($type.'.show', $this->slug);
        }

        return $url;
    }

    public function isBuilderPublished() {
        return $this->builder_status === 'published';
    }
}
