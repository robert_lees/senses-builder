<?php

namespace Senses\Builder;

class Builder {

    protected static $models = [];  //lookup for type => model class.
    protected $blocks;
    protected $html = "";
    protected $css = "";
    protected $processed = false;

    public function __construct(array $blocks)
    {
        $this->blocks = $blocks;
    }

    public function process() {
        $html = "";
        $css = "";
        foreach($this->blocks as $block) {
            $builderBlock = new BuilderBlock($block['template'], $block['value']);
            $builderBlock->process();
            $html .= $builderBlock->getHtml();
            $css .= $builderBlock->getCss();
        }

        $this->html = $html;
        $this->css = $css;
        $this->processed = true;
    }

    public function getHtml($forceRefresh = false) {
        if(!$this->processed || $forceRefresh) {
            $this->process();
        }

        return $this->html;
    }

    public function getCss($forceRefresh = false) {
        if(!$this->processed || $forceRefresh) {
            $this->process();
        }

        return $this->css;
    }

    public static function setModels(array $models) {
        self::$models = $models;
    }

    public static function getModels() {
        return self::$models;
    }

    public static function getModelKeys() {
        return array_keys(self::$models);
    }

    public static function getModelKey($model) {
        $modelKeys = array_flip(self::$models);
        if(isset($modelKeys[get_class($model)])) {
            return $modelKeys[get_class($model)];
        }
        return null;
    }

    public static function getModel($type, $id) {
        $modelClass = self::$models[$type];

        if(!isset($modelClass)) {
            return null;
        }

        return $modelClass::findOrFail($id);
    }
}