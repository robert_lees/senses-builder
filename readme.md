# Senses Builder

## About
Adds the ability to create html templates using predefined templates to models.

## Setup
You can install the package via composer:

```php
"repositories":[
     {
         "type": "vcs",
         "url" : "git@bitbucket.org:robert_lees/senses-builder.git"
     }
 ]
```

### Config
```php
php artisan vendor:publish --provider="Senses\Builder\BuilderServiceProvider" --tag="senses-builder-config"
```

You can override the usage of files, this will affect the migration generation too

### Migrations
Publish and run the migration
```php
php artisan vendor:publish --provider="Senses\Builder\BuilderServiceProvider" --tag="senses-builder-migrations"
php artisan migrate
```

Update model migrations to ensure builder fields are added. 
```php 
$table->sensesBuilderFields();
```

### Models
Add trait to models
```php 
use HasSensesBuilder;
``` 

Register models in AppServiceProvider //try to remove this requirement

### Routes
Add route macro
```php 
Route::sensesBuilder('senses-builder');
```

### Assets
Publish the vendor files
```php
php artisan vendor:publish --provider="Senses\Builder\BuilderServiceProvider" --tag="senses-builder-assets" --force
```

Add these to your laravel mix

### Views
```php
php artisan vendor:publish --provider="Senses\Builder\BuilderServiceProvider" --tag="senses-builder-views" --force
```

#### Usage

```blade
@include('senses-builder::run', ['model' => $page])
```

This will handle showing editor for admin, or defaulting to static view of  html/css

### Fontawesome

While the js/sass is still part of your projects build system, include this for fontawesome
```html
    <script src="https://kit.fontawesome.com/ec7b70e4e6.js" crossorigin="anonymous"></script>
```

### Custom file handling
 Disable files via config, this prevents migrations from generating and will disable file routes.

 You must then provide these routes for files

//expecting url of /api/senses-builder/files (currently prefix from route helper isn't supported)
 ```php 
    Route::get('files', [FileController::class, 'index']);
    Route::post('files', [FileController::class, 'store']);
 ```