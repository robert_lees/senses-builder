@isset($model)
    @if(request()->query('building') == 1)
        <senses-builder :model='@json($model->builder_data)'></senses-builder>
    @else
        @if($model->isBuilderPublished())
            @include('senses-builder::show', ['model' => $model])
        @endif
    @endif
@endif