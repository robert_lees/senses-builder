import Vue from 'vue';
import LaravelPagination from 'laravel-vue-pagination';
import Toasted from 'vue-toasted';
import SensesBuilder from './components/SensesBuilder.vue';
import SensesBuilderErrors from './components/SensesBuilderErrors.vue';
import SensesBuilderDropzone from './components/SensesBuilderDropzone.vue';

Vue.use(Toasted);
Vue.component('senses-builder', SensesBuilder);
Vue.component('senses-builder-errors', SensesBuilderErrors);
Vue.component('senses-builder-dropzone', SensesBuilderDropzone);
Vue.component('senses-builder-pagination', LaravelPagination);

