export default {
    props: {
        values:{
            type:[Array, Object],
        }
    },

    watch:{
        values(newVal, oldVal) {
            this.setFields();
        }
    },
    data() {
        return {
            name: this.$options._componentTag,
            fields:{}
        }
    },

    mounted() {
        this.setFields();
        //set listener for aside
    },

    destroyed() {
        //remove listener for aside
    },

    methods:{
        setFields() {
            //Arrays get stored for blocks without values, so lets ignore.
            if(this.values && !Array.isArray(this.values)) {
                this.setFieldsFromValues(this.fields, this.values);
            }
        },

        setFieldsFromValues(fields, data) {
            Object.keys(data).forEach(key => {
                if(fields[key]) {
                    if(fields[key].type === "group") {
                        this.setFieldsFromValues(fields[key].value, data[key]);
                    }
                    else {
                        fields[key].value = data[key];
                    }
                }
            });
        },

        setStyle(fieldName) {

            let styles = {};
            const field = this.fields[fieldName];

            if(field.type === "group") {
                const styleFields = Object.values(field.value).filter(this.isStyleTypeField);

                styleFields.forEach(styleField => {
                    styles[styleField.type] = styleField.value;
                });
            }
            else if(this.isStyleTypeField(field)) {
                styles[field.type] = field.value;
            }

            return styles;
        },

        isStyleTypeField(field) {
            return ["backgroundColor", "color"].includes(field.type);
        },

        getFields() {
            return this.fields;
        },

        getValues() {
            return this.getSubValues(this.fields);
        },

        getSubValues(fields) {
            let values = {};
            Object.keys(fields).forEach((key) => {
                const field = fields[key];
                if(field.type !== "group") {
                    values[key] = field.value;
                }
                else {
                    values[key] = this.getSubValues(field.value);
                }
            });
            return values;
        }
    }
}

