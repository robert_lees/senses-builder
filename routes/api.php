<?php
use Senses\Builder\Http\Controllers\TemplateBlockController;
use Senses\Builder\Http\Controllers\SensesBuilderController;
use Senses\Builder\Http\Controllers\FileController;


Route::get('template-blocks/{template_block}/image', [TemplateBlockController::class, 'showImage'])->name('senses-builder.template-blocks.block-image');
Route::get('template-blocks', [TemplateBlockController::class, 'index'])->name('senses-builder.template-blocks.index');
Route::get('template-blocks/categories', [TemplateBlockController::class, 'listCategories'])->name('senses-builder.template-blocks.categories');

Route::post('/', [SensesBuilderController::class, 'store'])->name('senses-builder.store');

if(config('senses-builder.enable_files', true)) {
    Route::get('files', [FileController::class, 'index'])->name('senses-builder.files.index');
    Route::post('files', [FileController::class, 'store'])->name('senses-builder.files.store');
}